# Explainable ML Engine
> Prerequisites: [Docker](https://docs.docker.com/get-docker/), [kubectl](https://kubernetes.io/docs/tasks/tools/)

## Setup
* [Install Go >= 1.15.5 ](https://golang.org/doc/install)
    ```bash
    # check configuration (check PATH e.g., /usr/local/go/bin/)
    go version
    ```
* [Install Miniconde (Python) and create evironment:](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html#regular-installation)
    ```bash
    conda create --name emle-env pip python=3.7
    conda activate emle-env
    pip install -r requirements.txt
    ```
* [Install Kind and intiale cluster](https://kind.sigs.k8s.io/docs/user/quick-start/) 
    ```bash
    GO111MODULE="on" go get sigs.k8s.io/kind@v0.11.1
    kind create cluster --image=kindest/node:v1.19.11@sha256:7664f21f9cb6ba2264437de0eb3fe99f201db7a3ac72329547ec4373ba5f5911
    ```

* [Clone KFServing repository. Make sure to use release 0.6 (after 0.6 kfserving has been renamed to kserve):](https://github.com/kserve/kserve/tree/release-0.6)
    ```
    mkdir tmp
    git clone https://github.com/kserve/kserve.git --branch release-0.6 tmp/kfserving
    ```

* Install Istio for Knative and KFServing for `demo` usage:

    ```bash
    cd tmp/kfserving
    ./hack/quick_install.sh
    ```
* Create demo namespace
    ```bash
    kubectl create namespace explainable-ml-engine-demo
    ```

## Deploy model

* [Train model and explainer](./1_income_model_and_explainer.ipynb)
* [Deploy model and explainer with KFServing Python client](./2_inference_server.ipynb). More background on KFServing can be found here: https://github.com/kubeflow/kfserving/tree/master/python/kfserving

    ![](/media/emle.png)

## Make prediction

* [Interact with Inferance server API](./3_test_api.ipynb)

```bash
INGRESS_GATEWAY_SERVICE=$(kubectl get svc --namespace istio-system --selector="app=istio-ingressgateway" --output jsonpath='{.items[0].metadata.name}')
kubectl port-forward --namespace istio-system svc/${INGRESS_GATEWAY_SERVICE} 8080:80
```

```bash
curl -v -H "Host: income-classifier.explainable-ml-engine-demo.example.com" http://localhost:8080/v1/models/income-classifier:predict -d '{"instances":[[39, 7, 1, 1, 1, 1, 4, 1, 2174, 0, 40, 9]]}'
```
